# Pocket SNN-v2 Vue.js

## Principais libs utilizadas:
* vue-router - roteador
* vuex - gerenciador de estados
* vuecity - framework de componentes material design
* axios - biblioteca para requisições http

Recursos:
* Login/Registro Rest;
* Cursos Graphql;

Melhorias:
* Testes (Mocha + Chai)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
