import Api from "../services/Api";

export default {
  fetchCourses() {
    var data = {
      query: `
      {
        Course(searchParam: {pagination: {page: 1, perPage: 20}}) {
          rows {
            id
            name
            user {
              id
              username
            }
          }
        }
      }`
    };
    return Api().post("/graphql", data);
  },

  createCourse(course) {
    var data = {
      query: `
      mutation {
        upsertCourse(input:{name:"${course.name}"}){
          id
          name
          user{
            id
            username
          }
        }
      }
      `
    };
    return Api().post("/graphql", data);
  }
};
