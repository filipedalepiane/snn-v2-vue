import axios from "axios";
import store from "../store";

export default () => {
  const instance = axios.create({
    baseURL: "http://localhost:3333",
    timeout: 5000,
    headers: {
      Authorization: `Bearer ${store.state.authentication.token}`
    }
  });
  return instance;
};
