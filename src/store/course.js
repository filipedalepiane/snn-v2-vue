import Vue from "vue";
import CourseService from "../services/CourseService";

export default {
  namespaced: true,
  state: {
    courses: [],
    newCourseName: ""
  },
  actions: {
    async fetchCourses({ commit }) {
      try {
        const response = await CourseService.fetchCourses();
        commit("setCourses", response.data.data.Course.rows);
      } catch (error) {
        console.log(error);
      }
    },

    async createCourse({ commit, state }) {
      try {
        const response = await CourseService.createCourse({
          name: state.newCourseName
        });
        commit("appendCourse", response.data.data.upsertCourse);
        commit("setNewCourseName", null);
      } catch (error) {
        console.log(error);
      }
    },

    async saveCourse({ commit }, course) {
      try {
        Vue.set(course, "isEditMode", false);
        commit("unsetEditMode", course);
      } catch (error) {
        console.log(error);
      }
    }
  },

  getters: {
    totalCourses(state) {
      return state.courses.length;
    }
  },
  mutations: {
    setNewCourseName(state, name) {
      state.newCourseName = name;
    },
    appendCourse(state, course) {
      state.courses.push(course);
    },
    setCourses(state, courses) {
      state.courses = courses;
    },
    setCourseName(state, { course, name }) {
      course.name = name;
    },
    setEditMode(state, course) {
      Vue.set(course, "isEditMode", true);
    },
    unsetEditMode(state, course) {
      Vue.set(course, "isEditMode", false);
    }
  }
};
