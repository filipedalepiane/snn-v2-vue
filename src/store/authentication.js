import router from "../router/index";
import AuthenticationService from "../services/AuthenticationService";

export default {
  namespaced: true,

  state: {
    registerUsername: "",
    registerEmail: "",
    registerPassword: "",
    registerError: null,
    loginEmail: "",
    loginPassword: "",
    loginError: null,
    token: null
  },

  actions: {
    logout({ commit }) {
      commit("setToken", null);
      router.push("/login");
    },

    async register({ state, commit }) {
      try {
        commit("setRegisterError", null);
        const response = await AuthenticationService.register({
          username: state.registerUsername,
          email: state.registerEmail,
          password: state.registerPassword
        });
        commit("setToken", response.token);
        router.push("/");
      } catch (error) {
        commit("setRegisterError", "Erro ao registrar a conta");
      }
    },

    async login({ state, commit }) {
      try {
        commit("setLoginError", null);
        const response = await AuthenticationService.login({
          email: state.loginEmail,
          password: state.loginPassword
        });
        commit("setToken", response.data.data.token);
        router.push("/");
      } catch (error) {
        commit("setLoginError", "Erro ao efetuar o login");
      }
    }
  },

  getters: {
    isLoggedIn(state) {
      return !!state.token;
    }
  },

  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    setRegisterError(state, error) {
      state.registerError = error;
    },
    setRegisterUsername(state, username) {
      state.registerUsername = username;
    },
    setRegisterEmail(state, email) {
      state.registerEmail = email;
    },
    setRegisterPassword(state, password) {
      state.registerPassword = password;
    },
    setLoginEmail(state, email) {
      state.loginEmail = email;
    },
    setLoginPassword(state, password) {
      state.loginPassword = password;
    },
    setLoginError(state, error) {
      state.loginError = error;
    }
  }
};
