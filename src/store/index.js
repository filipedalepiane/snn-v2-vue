import Vue from "vue";
import Vuex from "vuex";

import authentication from "./authentication";
import course from "./course";

Vue.use(Vuex);

export default new Vuex.Store({
  strict: true,
  state: {
    baseUrl: "/"
  },
  mutations: {},
  actions: {},
  modules: {
    authentication,
    course
  },
  plugins: []
});
